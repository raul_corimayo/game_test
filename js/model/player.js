function Player(){

	this.x = 0;
	this.y = 350;
	this.vx = 0;
	this.ax = 0;
	this.axMax = 0.2;
	this.dx = 64;
	
	this.dy = 64;
	this.vy = 0;
	this.resistencia = 0.08;

	this.air = true;
	this.air_time = 0;
	this.air_fase = 0;
	
	this.img = getImage('images/img.png');
	this.ball = getImage('images/man.png');

}


Player.prototype.update = function(){
	if(teclado[kleft]){
		this.ax = -this.axMax;
	}else if(teclado[kright]){
		this.ax = this.axMax;
	}else{
		this.ax = 0;
	}
	
	this.ax -=this.vx*this.resistencia;
	this.vx += this.ax;
	this.x += this.vx;

	if(teclado[ka]){
		if(this.air == false){
			this.vy = -Math.abs(this.vx/2)-8;		
			this.air_time = 0;
			this.air = true;		
		}
	}
	
	if(teclado[ks]){
		this.axMax = 0.4;
	}else{	
		this.axMax = 0.2;0.2
	}
	
	if(teclado[kd]){}
	if(teclado[kenter]){}
	if(this.air){
		if(teclado[ka]){
			this.vy += 0.25;
		}else{
			this.vy += 1.0;
		}
		this.y += this.vy;
		if(480 < this.y + this.dy){
			this.air = false;
			this.vy = 0;
			this.y = 480 - this.dy;
		}
	}else{
		this.air_time++;
		var vx = Math.abs(this.vx);
		if(vx!=0){
			if(10/vx<this.air_time){
				this.air_time = 0;
				this.air_fase += vx/this.vx;
				if(7<this.air_fase){
					this.air_fase = 0;
				}
				if(this.air_fase<0){
					this.air_fase = 7;
				}
			}
		}	
	}
}



Player.prototype.draw = function(){
	if(!this.air){
		var time = this.air_fase;
		ctx.drawImage(this.ball,32*time,32,32,32,this.x, this.y, this.dx, this.dy);
	}else{
		ctx.drawImage(this.img,this.x, this.y, this.dx, this.dy);
	}
}