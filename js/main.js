//Variables globales
var ctx;
var teclado;
var player;
var ancho;
var alto;
var fps;
var globalTime;

//Definicion de funciones
function setIteracionIntervalo(){
	window.setInterval(gameloop,1000/fps);
}
function gameloop(){
	update();
	draw();
}

function draw(){
	ctx.clearRect(0,0,ancho,alto);
	ctx.fillText(globalTime+" - "+player.air+" "+player.vy+" "+player.x, 20, 20);
	player.draw();
}
function update(){
	globalTime++;
	player.update();
}
function loadKeyEvents(){
	addEvent(document, "keydown", function(e){
		teclado[e.keyCode]=true;
		//alert(e.keyCode);
	});
	addEvent(document, "keyup", function(e){
		teclado[e.keyCode]=false;
	});
	function addEvent(element, eventName, func){
		if(element.addEventListener){
			element.addEventListener(eventName, func, false);
		}else if(element.attachEvent){
			element.attachEvent(elementName, func);
		}
	}
}

function main(){
	fullScreen();
	inicializar();
	loadKeyEvents();
	setIteracionIntervalo(fps);
}

function inicializar(){
	var canvas = document.getElementById('game');
	ancho = canvas.width;
	alto  = canvas.height;
	ctx = canvas.getContext('2d');
	//alert(ancho+" : "+alto);

	teclado = {};
	player = new Player();
	fps = 60;
	globalTime = 0;
}

function fullScreen(){
	window.onload = maxWindow;
    function maxWindow() {
        window.moveTo(0, 0);
        if (document.all) {
            top.window.resizeTo(screen.availWidth, screen.availHeight);
        }
        else if (document.layers || document.getElementById) {
            if (top.window.outerHeight < screen.availHeight || top.window.outerWidth < screen.availWidth) {
                top.window.outerHeight = screen.availHeight;
                top.window.outerWidth = screen.availWidth;
            }
        }
    }
}


